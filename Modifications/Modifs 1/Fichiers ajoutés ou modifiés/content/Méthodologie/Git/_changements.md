---
title: "Enregistrer des changements"
weight: 4
date: 2022-01-11T14:19:27Z
draft: false
---

### 
La notion d'enregistrement est plus complexe et flexible dans Git qu'avec un système de fichiers traditionnel. Pour bien le comprendre, il faut savor que Git maintient 3 états internes:  
* le répertoire de travail;
* l'index;
* l'historique des commits.

Le répertoire de travail est un espace synchronisé avec le système de fichier de votre poste de travail. Chaque modification (ajout, suppression, modification d'un fichier) se répercute dans le répertoire de travail.  

L'index correspond à l'ensemble des fichiers sélectionnés pour faire partie du prochain commit.  

La commande git commit capture un instantané des changements indexés. Tous les commits sont gardés dans un historique. Git garde un pointeur sur la référence d'un commit courant, appelé HEAD. En temps normal, HEAD est positionné sur le dernier commit effectué.  

La commande *status* affiche l'état de Git:  
![git status 1](http://ml.cm9.ca/cours3d1/images/gitStatus1.png)  
Le statut indique que le répertoire de travail ne contient aucun changment, l'index est vide et la branche est à jour avec la branche équivalente sur origine.  

La commande *log* affiche l'historique des commmits exécutés:  
![git log 1](http://ml.cm9.ca/cours3d1/images/gitLog1.png)  

La commande affiche les commits en ordre chronologique inverse. On voit que la branche *main* du dépôt local est synchronisée avec celle du dépôt *origine*.  

Si on ajoute le fichier *Deuxieme.txt* au dossier et que l'on modifie le fichier *Premier.txt*, status affiche le résultat suivant:  
![git status 2](http://ml.cm9.ca/cours3d1/images/gitStatus2.png)  
Git a détecté que le fichier *Premier.txt* a été modifié. La couleur rouge indique que le fichier n'a pas été indexé. *Deuxieme.txt* a le statut *untracked* qui indique que le fichier ne fait pas encore partie du répertoire de travail de Git.  

La commande *add* ajoute les fichiers spécifiés à l'index:  
![git status 3](http://ml.cm9.ca/cours3d1/images/gitStatus3.png)  
Le statut indique qu'un nouveau fichier est ajouté à l'index. Si on ajoute à l'index le fichier modifié, on obtient le résultat suivant:  
![git status 4](http://ml.cm9.ca/cours3d1/images/gitStatus4.png)  
Un commit enregistre toutes les modifications indexées dans l'historique des commits:  
![git status 5](http://ml.cm9.ca/cours3d1/images/gitStatus5.png)  
Le satut nous indique que le répertoire de travail ne contient pas de changements et l'index est vide. On voit aussi que que notre branche locale est 1 commit "en avance" sur la branche *origin/main*. Le dépôt distant n'est pas à jour par rapport à notre dépôt local. Un *push* de la branche *main* synchronisera *origin* avec notre dépôt local:
![git status 6](http://ml.cm9.ca/cours3d1/images/gitStatus6.png)  

