---
title: "Les bonnes pratiques"
weight: 10
date: 2022-01-11T14:19:27Z
draft: true
---

## Fonctions  
Les fonctions ne font qu'une chose  
	Une fonction fait une seule chose si c'est une suite d'étapes à un seul niveau d'abstraction 
Le nom de la fonction exprimer ce qu'elle fait  
Viser des fonctions d'au plus 20 lignes  
Viser des blocs d'instructions de 1 ligne  
Viser un niveau d'indentation d'au plus 2  

### Arguments d'une fonctions
Le moins d'arguments possible: pas plus de 2, 3 au maximum (plus il y a d'arguments, plus la fonction est difficile à tester)  
Pas d'argument de sortie

## Classes
Le code se lit de haut en bas, le plus important en premier  